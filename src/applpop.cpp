/***************************************************************************
                          applpop.cpp  -  Application dérivée de l'objet ApplPop
                          						spécifique aux populations
                             -------------------
    begin                : ven aug 14 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/




//#include "entetepop.h"

#include "applpop.h"

ApplPop::ApplPop ():Application(){
//cerr << _formatMatrice;
	_Pjeupop = 0;
	_niveau = 100; // niveau de structuration des populations (100 => dernier ensemble avant les individus)
}

ApplPop::~ApplPop() {
	if (_Pjeupop != 0) delete _Pjeupop;
}


/*
void ApplPop::fconcatenationpop() {
	// concatenation de jeux de populations
	Jeupop * PjeupopOriginal(_Pjeupop);

	cout << endl << "Lecture des populations a concatener :" <<endl;

	_Pjeupop = 0;
//cerr << "coucou" <<endl;
	if (!litGenepop()) {
//cerr << "coucou" <<endl;
		_Pjeupop = PjeupopOriginal;
//cerr << "coucou" <<endl;
		return;
	}

	try {
//cerr << PjeupopOriginal->_commentaires[0];
//cerr << _Pjeupop->_commentaires[0];
//cerr << "concat avant" <<endl;
		*_Pjeupop = *PjeupopOriginal + *_Pjeupop;
//cerr << "concat apres" <<endl;
//cerr << _Pjeupop->_commentaires.size(); //<< _Pjeupop->_commentaires[1];
	}
	catch (Jeupop::Anomalie pb) {
		cout << endl << "il y a eu une erreur de type: " << pb.le_pb << endl;

		_Pjeupop = PjeupopOriginal;
	}
}
*/



bool ApplPop::litGenepop() {
	Jeupop * Poldjeupop;
	bool ok(false);

	while (_fichier.is_open() == 0) {
		cout << _("Name of input file (Populations or Genepop format) ?") << endl;
		cin >> _nomFichier;

		_fichier.open(_nomFichier.c_str(), ios::in);
		_fichier.clear();
	}

	try {
		Poldjeupop = _Pjeupop;
//cerr << "ApplPop::litgenepop " <<endl;
		_Pjeupop = new Jeupop();
//cerr << "ApplPop::litgenepop " <<endl;
		_fichier >> *(_Pjeupop);
//cerr << "ApplPop::litgenepop fin >>" <<endl;
		_fichier.close();
		_fichier.clear();
		ok = true;
	}
	catch (Jeupop::Anomalie pb){
		_fichier.close();
		_fichier.clear();
		delete _Pjeupop;
		_Pjeupop = Poldjeupop;
		Poldjeupop = 0;
		ok = false;

		switch (pb.le_pb) {
		case 4:
			cout << _("The file format seems incorrect...") << endl;
			break;
		}
	}

	if (Poldjeupop != 0) delete Poldjeupop;
	return(ok);
}



bool ApplPop::iPopulations() {

	if (_Pjeupop == 0) {
		if(litGenepop()) return(true);
		else return(false);
	}
	return(true);
}


void ApplPop::affPubEntree() const {

		cout << endl << endl;
		cout << "**************************************" << endl;
		cout << "*                                    *" << endl;
		cout << "*      langella@moulon.inra.fr       *" << endl;
		cout << "* http://www.cnrs-gif.fr/pge/bioinfo *" << endl;
		cout << "**************************************" << endl;
		cout << endl << endl;
}



/** Demande à l'utilisateur de choisir une population.
retourne le numéro dans _tabPpop */
unsigned long ApplPop::DemandePopulation(char laquestion[]) const{
	signed long numpop(-1);
	unsigned long i;
	string rep;

	while ((numpop < 0) || ((unsigned long) numpop > _Pjeupop->get_nbpop())) {
		cout << _("Please, choose a population among these ones:") << endl;
		for (i=0; i <  _Pjeupop->get_nbpop(); i++) cout << _Pjeupop->get_nompop(i) << " ";
		cout << endl;

		cout << endl << laquestion << endl;
		cin >> rep;
		try {
			numpop = _Pjeupop->get_numpop(_Pjeupop->get_Ppop(rep));
		}
		catch (Jeupop::Anomalie le_pb) {
			cout << _("the population ") << rep << _(" has not been found.") << endl;		
			numpop = -1;	
		}
	}
	return ((unsigned long) numpop);

}

void ApplPop::set_groupes_individus() {
	// crée des groupes d'individus par Populations
	if (_Pjeupop == 0) return;
	unsigned int nbpop, nbind, i, j;

//cerr << "ApplPop::set_groupes_individus() debut" << endl;
	Titre noms_niveaux;

	try {
//cerr << "ApplPop::set_groupes_individus() rempli" << endl;
		_Pjeupop->f_rempliVcalcStrucPop(_niveau);
//cerr << "ApplPop::set_groupes_individus() noms" << endl;
		//_Pjeupop->get_nomniveauxstruc(noms_niveaux);
	}
	catch (Jeupop::Anomalie pb) {
  	cerr << "ApplPop::set_groupes_individus() anomalie Jeupop " << pb.le_pb << endl;
	}

	nbpop = _Pjeupop->get_nbstrucpopVcalc();

	_tab_groupes.resize(nbpop);
	_tab_noms_groupes.resize(nbpop);
	_tab_couleurs_groupes.resize(nbpop);

	for (i = 0; i < nbpop; i++) {
		_tab_noms_groupes[i] = _Pjeupop->get_PstrucpopVcalc(i)->get_nom();
		_tab_couleurs_groupes[i] = _Pjeupop->get_PstrucpopVcalc(i)->get_couleur();

		nbind = _Pjeupop->get_PstrucpopVcalc(i)->get_nbind();
		_tab_groupes[i] = "";
		for (j = 0; j < nbind; j++) {
			_tab_groupes[i] += "'";
			_tab_groupes[i] += _Pjeupop->get_PstrucpopVcalc(i)->get_nomind(j);
//cerr << "ApplPop::set_groupes_individus() get_nomind" << endl;
			_tab_groupes[i] += "'";
//cerr << _tab_groupes[i] << endl;
			//_tab_couleurs_groupes[i] = Couleur::get_couleur_diff();
			if (j != (nbind -1)) _tab_groupes[i] += " ";
		
    }
	}
//cerr << "ApplPop::set_groupes_individus() fin";
	
}

void ApplPop::set_groupes_populations() {
	// crée des groupes d'individus par Populations
	if (_Pjeupop == 0) return;

	unsigned int nbstrucpop, i,j;
	reset_groupes();
	MetaPop * Pmetapop;
	Vecteur<MetaPop *> tabPmetapop;

	_Pjeupop->f_rempliVcalcStrucPop(_niveau);

	nbstrucpop = _Pjeupop->get_nbstrucpopVcalc();

	//balayage des strucpop pour voir si il n'existe pas une métastructure
	// => groupe de strucpop
	for (i = 0; i < nbstrucpop; i++) {
		if (_Pjeupop->get_PstrucpopVcalc(i)->get_niveau() < 2) {
			 //pas de metastructure
		}
		else {

			Pmetapop = _Pjeupop->get_PstrucpopVcalc(i)->get_Pmetapop();

			if (!(tabPmetapop.Existe(Pmetapop))) tabPmetapop.push_back(Pmetapop);
		}
	}
	
	_tab_groupes.resize(tabPmetapop.size());
	_tab_noms_groupes.resize(tabPmetapop.size());
	_tab_couleurs_groupes.resize(tabPmetapop.size());

	for (i = 0; i < tabPmetapop.size(); i++) {
		_tab_noms_groupes[i] = tabPmetapop[i]->get_nom();
		_tab_couleurs_groupes[i] = tabPmetapop[i]->get_couleur();

		nbstrucpop = tabPmetapop[i]->get_nbpop();
		_tab_groupes[i] = "";
		for (j = 0; j < nbstrucpop; j++) {
			_tab_groupes[i] += "'";
			_tab_groupes[i] += tabPmetapop[i]->get_nompop(j);
//cerr << "ApplPop::set_groupes_individus() get_nomind" << endl;
			_tab_groupes[i] += "'";
//cerr << _tab_groupes[i] << endl;
			//_tab_couleurs_groupes[i] = Couleur::get_couleur_diff();
			if (j != (nbstrucpop -1)) _tab_groupes[i] += " ";
		
    }

	}
	

}

void ApplPop::set_groupes_arbre(ArbrePlus & larbre) const {
//	larbre._titre = _temp;
	if (_tab_groupes.size() == 0) return;

	unsigned int i, nbgroupes(_tab_groupes.size());

	larbre.f_tri_ind_alpha();

	for (i = 0; i < nbgroupes; i ++) {
    larbre.ajouter_groupe(_tab_groupes[i], _tab_couleurs_groupes[i], _tab_noms_groupes[i]);
	}
}
