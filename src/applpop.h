/***************************************************************************
                          applpop.h  -  Application dérivée de l'objet ApplPop
                          						spécifique aux populations
                             -------------------
    begin                : ven aug 14 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef APPLPOP_H
#define APPLPOP_H

#include "jeupopexp.h"
#include "jeupop.h"
#include "arbreplus.h"
#include "distgnt.h"

//#include "matrices.h"
#include "applications.h"

typedef biolib::arbres::ArbrePlus ArbrePlus;

class ApplPop:public Application {
public:

	ApplPop();
	~ApplPop();
  /** Demande à l'utilisateur de choisir une population.
retourne le numéro dans _tabPpop */
  unsigned long DemandePopulation(char []) const;

	void set_groupes_individus();
	void set_groupes_populations();
	void set_groupes_arbre(ArbrePlus & larbre) const;
	void reset_groupes() {_temp.resize(0);_tab_groupes.resize(0);_tab_noms_groupes.resize(0);_tab_couleurs_groupes.resize(0);};
	
protected:
	
	virtual bool litGenepop();

	virtual void affPubEntree() const;
	virtual bool iPopulations();

	Jeupop* _Pjeupop;
	vector<string> _tab_groupes;
	vector<string> _tab_noms_groupes;
	vector<string> _tab_couleurs_groupes;

	unsigned int _niveau;

	Titre _temp;

};

#endif

