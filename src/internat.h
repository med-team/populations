 /***************************************************************************
                          internat.h  -  description
                             -------------------
    begin                : Thu Sep 21 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

//internationalisation
#ifndef INTERNAT_H
# define INTERNAT_H
# ifdef MINGW32
#  define OLIV_INTERNAT 0
# endif
# ifdef OLIV_INTERNAT
#   include <locale.h>
//#   include <libintl.h>
#  define _(String) (String)
#  define gettext(String) (String)
#  define textdomain(String) (String)
#  define setlocale(String1, String2) (String1, String2)
#  define bindtextdomain(String1, String2) (String1, String2)
#  define LOCALEDIR ""
//#  define LC_ALL ""
//#  define LC_NUMERIC ""
#  define PACKAGE ""
# else
#  ifndef _LOCALE_H
#   include <locale.h>
#   ifndef LOCALEDIR
#    define LOCALEDIR "/usr/share/locale"
#   endif
#  endif
#  ifndef _LIBINTL_H
#   include <libintl.h>
#   ifndef _
#    define _(String) gettext (String)
#   endif
#  endif
# endif
#endif


