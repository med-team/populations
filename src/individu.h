/***************************************************************************
                          individu.h  -  description
                             -------------------
    begin                : Thu Sep 14 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// individu
#ifndef INDIVIDU_H
#define INDIVIDU_H

#include "allele.h"

using namespace biolib::vecteurs;

class Individu {
public :
//	
	Individu (Population *);
	Individu (Population * Ppop, const string& nom);
	Individu (Population *, const ConstGnt &, int);

	Individu (const Individu &, Population *);

//	Individu (int nball=0);
	~Individu();

	const Individu& operator= (const Individu &);
	bool operator== (const Individu &) const;
	bool r_estnul () const;
	bool r_esthetero (unsigned long locus) const;
	inline unsigned long r_nbcopall(Allele * Pall) const {return(_tabPall.getNbOccurence(Pall));};
	unsigned long r_nballnonnuls(unsigned long locus, unsigned int nploidie) const;

	inline unsigned long get_nballnuls () const;
	unsigned long get_nballnuls (unsigned long) const;
  void set_Pallele (Allele * Pall, unsigned int pos);

	inline const string& get_nom() const {return(_nom);};
  unsigned int get_nploidie() const {return(_nploidie);};
	const Vecteur<Allele*>& get_tabPall() const {return(_tabPall);};
	const string& get_nom_all(unsigned long numlocus, unsigned long numall) const;

	void resize_alleles();

	void affiche (ostream&, int) const;

	friend class Population;
	friend class StrucPop;
	friend class DistancesGnt;
	friend class Allele;
	friend class Jeupop;

	void ifFusionnerIndividu(const Individu& individu);
	void ifPlacerAllele(Locus* Ploc, long position, const Allele * Pall);

  void oPopulationsXML(unsigned int id, ostream & sortie, ostream & infos) const;

private :
	void creation (Population * Ppop);
  /** Suppresssion d'un locus */
  void supprLocus(unsigned int);

	string _nom;
//	char _nom[20];
	Population * _Ppop;
//	unsigned long _nball; //nb allèles= nb de loci* n ploidie
	unsigned int _nploidie;
	Vecteur<Allele*> _tabPall;

public:
	struct Anomalie{
		// 1-> copie impossible: pas le meme nombre d'alleles
		// 11-> numero de locus inexistant
		// 12-> numéro d'allele inexistant
		// 13-> un allele nul fausse la determination de l'heterozygotie de l'individu pour un locus
		int le_pb;
		Anomalie (int i):le_pb(i){};
	};

};


inline unsigned long Individu::get_nballnuls () const {
	//retourne le nombre d'alleles nuls de l'individu
	unsigned long taille (_tabPall.size());
	unsigned long i;
	unsigned long resultat(0);

	for (i = 0; i < taille; i++) {
		if (_tabPall[i]->_miss) resultat++;
	}
	return(resultat);
}

#endif

