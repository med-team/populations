
/***************************************************************************
                          population.h  -  Librairie d'objets permettant de manipuler des données
                          						spécifiques aux populations
                             -------------------
    begin                : ven sep 01 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// population
#ifndef POPULATION_H
#define POPULATION_H

#include "strucpop.h"
#include "metapop.h"
#include "vecteurs.h"

class Population: public StrucPop {
public:
//	Population(Jeupop * pdonnees, long nbind);
//	Population(Jeupop * pdonnees, MetaPop * Pmetapop, long nbind);
	Population(Jeupop * pdonnees);
	Population(MetaPop * Pmetapop);
//	Population(Jeupop * pdonnees, const StrucPop & popori): StrucPop(pdonnees,popori){};
//	Population(Jeupop * pdonnees, const Population & popori);
	Population(const StrucPop & popori, MetaPop * Pmetapop);
	virtual ~Population();

	bool DuType(const string &) const;

	void set_nploidie();
  void set_nploidie(unsigned int nploidie);

  Individu * new_individual(string & name, unsigned int nploidie);
	void AjouterIndividu(Individu * Pind);
	void get_nomniveauxstruc(Titre & nom_niveaux) const;
	unsigned int get_niveau() const;
	string get_nom_chemin() const;

	void f_rempliTabStrucPop(Vecteur<StrucPop*> &, unsigned int);

  /** Suppression de la Population */
  void SupprPop(StrucPop * Ppop);
	friend class Jeupop;
  /** Suppression d'un individu dans une Population */
  void SupprIndividu(Individu * Pind) ;

  void oPopulationsXML(unsigned int id, ostream & sortie, ostream & infos) ;
	
protected:
	virtual void ifAjouterIndividu(const Individu * Pind);


};

#endif
