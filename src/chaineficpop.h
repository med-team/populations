// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

//created:
//mer sep 06 14:23:39 CEST 2000 Langella <Olivier.Langella@pge.cnrs-gif.fr>


/***************************************************************************
 chaineficpop.h  -  Objet pour faciliter la lecture de fichiers genepop/populations
 -------------------
 begin                : ven sep 06 10:25:55 CEST 2000
 copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
 email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

#ifndef CHAINEFICPOP_H
#define CHAINEFICPOP_H

#include"vecteurs.h"

//typedef biolib::vecteurs::ChaineCar ChaineCar;

class ChaineFicPop: public biolib::vecteurs::ChaineCar {
public:
	ChaineFicPop(istream & fichier);
	~ChaineFicPop();

	int get_titre(biolib::vecteurs::Titre & titres);
	int get_locus(biolib::vecteurs::Titre & locus);

	int get_lignepop();
	int get_nompop(biolib::vecteurs::ChaineCar & nompop) const;
	int get_nomind(biolib::vecteurs::ChaineCar & nomind) const;
	int get_alleles(biolib::vecteurs::Titre & alleles) const;

	long get_numeroligne() const;

	bool estunepop();
	bool estunindividu() const;
	bool findefichier() const;
	bool get_boolnompop() const;

protected:
	void ajout_locus(biolib::vecteurs::Titre & locus) const;

	bool get_ligne();
	bool estuntitre();

	int _type_fichier; //1-> genepop 2-> populations 0-> indéterminé -1 -> non reconnu
	long _numeroligne;
	bool _danspop;
	bool _findefichier;
	bool _boolnompop;

	istream & _fichier;

	string _chainetemp;
	string _nompop;
public:
	struct Anomalie {
		// 1-> fin de fichier précoce
		// 2-> Le début de pop n'est pas indiqu
		// 3-> fichier XML

		int le_pb;
		Anomalie(int i) :
			le_pb(i) {
		}
		;
	};

};

#endif

