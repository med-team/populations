/***************************************************************************
                          fstat.h  -  bibliotheque permettant de calculer et de
																		présenter les indices de Wright
                             -------------------
    begin                : Wed Oct 11 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FSTAT_H
#define FSTAT_H

#include"jeupop.h"
#include"metapop.h"

// calculs et affichage de Fstat
class Fstat {
public :
	Fstat(MetaPop * Pmetapop);
	~Fstat();

	void f_affparlocus (ostream & sortie) const;
	//void f_calcFstFisFit (MatriceLD & matrice) const {f_calcFstFisFit (matrice, _Pmetapop);};
	void f_calcFstFisFit (JeuMatriceLD & matrices) const;

	void f_rempliVcalcStrucPop (unsigned int niveau) {
		_Pjeupop->f_rempliVcalcStrucPop(niveau);

		_tabPstrucpop = _Pjeupop->_VcalcStrucPop;
	}

private :
	void f_calcFstFisFit (MatriceLD & matrice, const MetaPop * Pmetapop) const;
	void f_calcFstFisFit (JeuMatriceLD & matrices, const MetaPop * Pmetapop) const;
	
	MetaPop * _Pmetapop;
	Jeupop * _Pjeupop;

	vector<StrucPop*> _tabPstrucpop;

public:

	struct Anomalie{

		int le_pb;
		Anomalie (int i):le_pb(i){};
	};


};
#endif

