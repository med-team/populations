
/***************************************************************************
                          MetaPop.h  -  Librairie d'objets permettant de manipuler des données
                          						spécifiques aux populations
                             -------------------
    begin                : ven sep 01 10:25:55 CEST 2000
    copyright            : (C) 2000 by Olivier Langella CNRS UPR9034
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// population

#ifndef METAPOP_H
#define METAPOP_H

#include "locus.h"
#include "strucpop.h"
#include "population.h"
#include "vecteurs.h"

using namespace biolib::vecteurs;

class MetaPop: public StrucPop {
public:
	MetaPop(Jeupop * pdonnees);
	MetaPop(MetaPop * pmetapopsup);
	MetaPop(const MetaPop & popori, MetaPop * Pmetapop, Jeupop * Pjeu);
	~MetaPop();

	bool DuType(const string & nom) const;
	friend class Jeupop;

	void AjouterIndividu(Individu * Pind);
	void AjouterPopulation(MetaPop * );
	void AjouterPopulation(Population * );

	MetaPop * NewMetaPop(const biolib::vecteurs::ChaineCar & nom);
  Population * new_population(string & nom);

	unsigned long get_nbpop() const {return(_tabPstrucpop.size());};
	unsigned long get_nbmetapop() const {return(_tabPmetapop.size());};
	const MetaPop * get_tabPmetapop(unsigned int i) const {return (_tabPmetapop[i]);};
	const string & get_nompop(unsigned int i) const {return(_tabPstrucpop[i]->get_nom());};
	void get_nomniveauxstruc(Titre & nom_niveaux) const;
	unsigned int get_niveau() const {return (_niveau_struc);};

	void set_nploidie();

	void f_rempliTabStrucPop(Vecteur<StrucPop*> &, unsigned int);

	long double f_Mheterozygotieobs(unsigned long locus) const;
	long double f_Mheterozygotieatt(unsigned long locus) const;
	long double f_Mheterozygotietotale(unsigned long locus) const;

	long double f_M_Fis(unsigned long locus) const;
	long double f_M_Fst(unsigned long locus) const;
	long double f_M_Fit(unsigned long locus) const;

  void oPopulationsXML(unsigned int id, ostream & sortie, ostream & infos) ;
	
  /** Retourne vrai, si l'allele n'est pas présent dans MetaPop */
  bool r_allelenonpresent(Allele * Pall) const;
protected:
  /** Suppression de la Population Ppop */
  void SupprPop(StrucPop * Ppop);
  MetaPop * get_Ptabmetapop(const StrucPop *) const;

	void ifAjouterIndividu(const Individu * Pind);

	void reset();

  /** réaffectation du nombre de locus
 */
  void set_nbloc();
	unsigned int _niveau_struc;

	vector<StrucPop *> _tabPstrucpop;

	vector<MetaPop *> _tabPmetapop;

};

#endif

