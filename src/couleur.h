/***************************************************************************
                          couleur.h  -  description
                             -------------------
    begin                : Mon Jun 4 2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef COULEUR_H
#define COULEUR_H


/**stockage de couleur (RGB ou nom en Anglais)
  *@author Olivier Langella
  */
#include "vecteurs.h"

//typedef biolib::vecteurs::ChaineCar ChaineCar;

class Couleur {
public:

	static const string & get_couleur_diff();

	Couleur(const biolib::vecteurs::ChaineCar & couleur);
	Couleur(unsigned long rouge, unsigned long vert, unsigned long bleu);
	~Couleur();

//	void set_rgb(unsigned long rouge, unsigned long vert, unsigned long bleu);

	bool EstNom() const {return(_boolnom);};
	bool EstRGB() const {return(_boolrgb);};

	void set_nom(const char * nom) {_boolrgb = false; _boolnom = true; _nom.assign(nom);};
	const string & get_nom() const {return(_nom);};
	unsigned long get_rouge() const {return(_rouge);};
	unsigned long get_vert() const {return(_vert);};
	unsigned long get_bleu() const {return(_bleu);};


	bool operator== (const Couleur &rval) const;

protected:

	static void f_rempli_tab_couleurs();

	static vector<string> _tab_couleurs;
	static unsigned int _pos_tab_couleurs;

	bool _boolrgb;
	bool _boolnom;

	string _nom;

	unsigned long _rouge;
	unsigned long _vert;
	unsigned long _bleu;

	
};

//unsigned int Couleur::_pos_tab_couleurs = 0;
/*
inline const string & Couleur::get_couleur_diff() {
	unsigned int i;

	if ( Couleur::_tab_couleurs.size() == 0 ) Couleur::f_rempli_tab_couleurs();
	i = Couleur::_pos_tab_couleurs;
	Couleur::_pos_tab_couleurs++;
	if (Couleur::_pos_tab_couleurs == Couleur::_tab_couleurs.size()) Couleur::_pos_tab_couleurs = 0;

	return (Couleur::_tab_couleurs[i]);
}
*/
/*
inline void Couleur::f_rempli_tab_couleurs() {
	_pos_tab_couleurs = 0;

	_tab_couleurs.push_back("red");
	_tab_couleurs.push_back("green");
	_tab_couleurs.push_back("blue");

}
*/
#endif


