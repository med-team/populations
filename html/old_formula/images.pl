# LaTeX2HTML 2K.1beta (1.48)
# Associate images original text with physical files.


$key = q/(y_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="\( y_{ij} \)">|; 

$key = q/{displaymath}a_{l}=frac{1}{2}sum_{u}left(tilde{p}_{1lu}-tilde{p}_{2lu}right)^{2}alpha}_{2l}right)}{4n_{1}n_{2}left(n_{1}+n_{2}-1right)}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="364" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="\begin{displaymath}
a_{l}=\frac{1}{2}\sum _{u}\left( \tilde{p}_{1lu}-\tilde{p}_{...
...\alpha }_{2l}\right) }{4n_{1}n_{2}\left( n_{1}+n_{2}-1\right) }\end{displaymath}">|; 

$key = q/{displaymath}Ds=-lnleft[frac{J_{XY}}{sqrt{J_{X}J_{Y}}}right]{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="144" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="\begin{displaymath}
Ds=-\ln \left[ \frac{J_{XY}}{\sqrt{J_{X}J_{Y}}}\right] \end{displaymath}">|; 

$key = q/(n_{c}=frac{left(rbar{n}-sum^{r}_{i=1}frac{n^{2}_{i}}{rbar{n}}right)}{left(r-1right)});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="143" HEIGHT="74" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="\( n_{c}=\frac{\left( r\bar{n}-\sum ^{r}_{i=1}\frac{n^{2}_{i}}{r\bar{n}}\right) }{\left( r-1\right) } \)">|; 

$key = q/{displaymath}R=frac{left(2x+y+zright)tilde{theta}_{L}^{2}-2left(x+zright)tilde{theta}_{L}+z}{1-2tilde{theta}_{L}+2tilde{theta}_{L}^{2}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="266" HEIGHT="50" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="\begin{displaymath}
R=\frac{\left( 2x+y+z\right) \tilde{\theta }_{L}^{2}-2\left(...
...theta }_{L}+z}{1-2\tilde{\theta }_{L}+2\tilde{\theta }_{L}^{2}}\end{displaymath}">|; 

$key = q/(X^{2});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="\( X^{2}\protect \)">|; 

$key = q/{displaymath}X^{2}=frac{sum^{r}_{j}sum^{m_{j}}_{i}2frac{left(x_{ij}-y_{ij}right)^{2}}{left(x_{ij}+y_{ij}right)}}{r}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="180" HEIGHT="50" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="\begin{displaymath}
X^{2}=\frac{\sum ^{r}_{j}\sum ^{m_{j}}_{i}2\frac{\left( x_{ij}-y_{ij}\right) ^{2}}{\left( x_{ij}+y_{ij}\right) }}{r}\end{displaymath}">|; 

$key = q/(tilde{theta}_{l}=frac{a_{l}}{a_{l}+b_{l}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="\( \tilde{\theta }_{l}=\frac{a_{l}}{a_{l}+b_{l}} \)">|; 

$key = q/{displaymath}DReynold=-ln(1-theta){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="168" HEIGHT="28" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="\begin{displaymath}
DReynold=-\ln (1-\theta )\end{displaymath}">|; 

$key = q/{displaymath}Dc=frac{2}{Pir}sum^{r}_{j}sqrt{2left((1-sum_{i}^{m_{j}}sqrt{x_{ij}y_{ij}}right)}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="258" HEIGHT="64" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="\begin{displaymath}
Dc=\frac{2}{\Pi r}\sum ^{r}_{j}\sqrt{2\left( (1-\sum _{i}^{m_{j}}\sqrt{x_{ij}y_{ij}}\right) }\end{displaymath}">|; 

$key = q/(tilde{p}_{lu}=sum^{r}_{i=1}frac{n_{i}tilde{p}_{ilu}}{rbar{n}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="126" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="\( \tilde{p}_{lu}=\sum ^{r}_{i=1}\frac{n_{i}\tilde{p}_{ilu}}{r\bar{n}} \)">|; 

$key = q/(x_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="\( x_{ij} \)">|; 

$key = q/{displaymath}Dr=frac{1}{r}sum^{r}_{j}sqrt{frac{sum^{m}_{i}left(x_{ij}-y_{ij}right)^{2}}{2}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="212" HEIGHT="63" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="\begin{displaymath}
Dr=\frac{1}{r}\sum ^{r}_{j}\sqrt{\frac{\sum ^{m}_{i}\left( x_{ij}-y_{ij}\right) ^{2}}{2}}\end{displaymath}">|; 

$key = q/{displaymath}a_{l}=frac{left[2sum^{r}_{i=1}n_{i}sum^{v_{l}}_{u=1}left(tilde{p}_{t)^{2}-left(r-1right)b_{l}right]}{2left(r-1right)n_{c}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="330" HEIGHT="56" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="\begin{displaymath}
a_{l}=\frac{\left[ 2\sum ^{r}_{i=1}n_{i}\sum ^{v_{l}}_{u=1}\...
...^{2}-\left( r-1\right) b_{l}\right] }{2\left( r-1\right) n_{c}}\end{displaymath}">|; 

$key = q/(z=sum^{m}_{l=1}a_{l}^{2});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="\( z=\sum ^{m}_{l=1}a_{l}^{2} \)">|; 

$key = q/(tilde{theta}_{L});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="\( \tilde{\theta }_{L} \)">|; 

$key = q/(C_{XY});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="\( C_{XY} \)">|; 

$key = q/{displaymath}Dm=frac{left(J_{X}+J_{Y}right)}{2}-J_{XY}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="175" HEIGHT="40" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="\begin{displaymath}
Dm=\frac{\left( J_{X}+J_{Y}\right) }{2}-J_{XY}\end{displaymath}">|; 

$key = q/{displaymath}D_{R}=-logleft(frac{C_{XY}}{V_{XY}}right){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="141" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="\begin{displaymath}
D_{R}=-\log \left( \frac{C_{XY}}{V_{XY}}\right) \end{displaymath}">|; 

$key = q/{displaymath}tilde{theta}_{L}=frac{2x+y-zpmsqrt{left(z-yright)^{2}+4x^{2}}}{2left(y-zright)}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="248" HEIGHT="57" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="\begin{displaymath}
\tilde{\theta }_{L}=\frac{2x+y-z\pm \sqrt{\left( z-y\right) ^{2}+4x^{2}}}{2\left( y-z\right) }\end{displaymath}">|; 

$key = q/(W_{Y}=frac{sum^{r}_{k}sum_{ineqj}left(left|i-jright|y_{ik}y_{jk}right)}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="190" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="\( W_{Y}=\frac{\sum ^{r}_{k}\sum _{i\neq j}\left( \left\vert i-j\right\vert y_{ik}y_{jk}\right) }{r} \)">|; 

$key = q/(J_{Y}=frac{sum^{r}_{j}sum^{m_{j}}_{i}y^{2}_{ij}}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="127" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="\( J_{Y}=\frac{\sum ^{r}_{j}\sum ^{m_{j}}_{i}y^{2}_{ij}}{r} \)">|; 

$key = q/{displaymath}D_{A}=1-frac{1}{r}sum^{r}_{j}sum^{m_{j}}_{i}sqrt{x_{ij}y_{ij}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="191" HEIGHT="57" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="\begin{displaymath}
D_{A}=1-\frac{1}{r}\sum ^{r}_{j}\sum ^{m_{j}}_{i}\sqrt{x_{ij}y_{ij}}\end{displaymath}">|; 

$key = q/(overline{P}_{SA_{X}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="\( \overline{P}_{SA_{X}} \)">|; 

$key = q/(mu_{Y_{j}}=sum_{i}iy_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="\( \mu _{Y_{j}}=\sum _{i}iy_{ij} \)">|; 

$key = q/{displaymath}b_{l}=2sum^{r}_{i=1}frac{n_{i}tilde{alpha}_{il}}{r(2bar{n}-1)}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="136" HEIGHT="53" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="\begin{displaymath}
b_{l}=2\sum ^{r}_{i=1}\frac{n_{i}\tilde{\alpha }_{il}}{r(2\bar{n}-1)}\end{displaymath}">|; 

$key = q/(overline{mu_{X}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="\( \overline{\mu _{X}} \)">|; 

$key = q/(J_{X}=frac{sum^{r}_{j}sum^{m_{j}}_{i}x^{2}_{ij}}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="128" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="\( J_{X}=\frac{\sum ^{r}_{j}\sum ^{m_{j}}_{i}x^{2}_{ij}}{r} \)">|; 

$key = q/(W_{X}=frac{sum^{r}_{k}sum_{ineqj}left(left|i-jright|x_{ik}x_{jk}right)}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="193" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="\( W_{X}=\frac{\sum ^{r}_{k}\sum _{i\neq j}\left( \left\vert i-j\right\vert x_{ik}x_{jk}\right) }{r} \)">|; 

$key = q/(mu_{Y_{j}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="\( \mu _{Y_{j}} \)">|; 

$key = q/{displaymath}ASD=frac{sum^{r}_{k}sum_{i,j}left(left(i-jright)^{2}x_{ik}y_{jk}right)}{r}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="238" HEIGHT="52" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="\begin{displaymath}
ASD=\frac{\sum ^{r}_{k}\sum _{i,j}\left( \left( i-j\right) ^{2}x_{ik}y_{jk}\right) }{r}\end{displaymath}">|; 

$key = q/{displaymath}Fst=frac{frac{left(J_{X}+J_{Y}right)}{2}-J_{XY}}{1-J_{XY}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="156" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="\begin{displaymath}
Fst=\frac{\frac{\left( J_{X}+J_{Y}\right) }{2}-J_{XY}}{1-J_{XY}}\end{displaymath}">|; 

$key = q/(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="\( \theta \)">|; 

$key = q/(y=sum^{m}_{l=1}b_{l}^{2});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="88" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="\( y=\sum ^{m}_{l=1}b_{l}^{2} \)">|; 

$key = q/{displaymath}Cp=frac{sum^{r}_{j}sum^{m_{j}}_{i}left|x_{ij}-y_{ij}right|}{2r}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="176" HEIGHT="44" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="\begin{displaymath}
Cp=\frac{\sum ^{r}_{j}\sum ^{m_{j}}_{i}\left\vert x_{ij}-y_{ij}\right\vert }{2r}\end{displaymath}">|; 

$key = q/(W_{XY}=frac{sum^{r}_{k}sum_{ineqj}left(left|i-jright|x_{ik}y_{jk}right)}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="202" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="\( W_{XY}=\frac{\sum ^{r}_{k}\sum _{i\neq j}\left( \left\vert i-j\right\vert x_{ik}y_{jk}\right) }{r} \)">|; 

$key = q/(mu_{X_{j}}=sum_{i}ix_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="\( \mu _{X_{j}}=\sum _{i}ix_{ij} \)">|; 

$key = q/(phi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="\( \phi \protect \)">|; 

$key = q/{displaymath}D_{SW}=W_{XY}-frac{left(W_{X}+W_{Y}right)}{2}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="201" HEIGHT="40" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="\begin{displaymath}
D_{SW}=W_{XY}-\frac{\left( W_{X}+W_{Y}\right) }{2}\end{displaymath}">|; 

$key = q/(C_{XY}=frac{1}{r-1}left(sum^{r}_{j}mu_{X_{j}}mu_{Y_{j}}-roverline{mu_{X}}overline{mu_{Y}}right));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="253" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="\( C_{XY}=\frac{1}{r-1}\left( \sum ^{r}_{j}\mu _{X_{j}}\mu _{Y_{j}}-r\overline{\mu _{X}}\overline{\mu _{Y}}\right) \)">|; 

$key = q/(overline{P}_{SA_{Y}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="\( \overline{P}_{SA_{Y}} \)">|; 

$key = q/(mu_{X_{j}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="\( \mu _{X_{j}} \)">|; 

$key = q/(C_{XY}<1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="\( C_{XY}&lt;1 \)">|; 

$key = q/(overline{mu_{Y}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="\( \overline{\mu _{Y}} \)">|; 

$key = q/(P_{SA_{I}}=frac{sum^{r}_{j}S}{2r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="\( P_{SA_{I}}=\frac{\sum ^{r}_{j}S}{2r} \)">|; 

$key = q/{displaymath}left(deltamuright)^{2}=frac{sum^{r}_{j}left(mu_{X_{j}}-mu_{Y_{j}}right)^{2}}{r}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="178" HEIGHT="47" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="\begin{displaymath}
\left( \delta \mu \right) ^{2}=\frac{\sum ^{r}_{j}\left( \mu _{X_{j}}-\mu _{Y_{j}}\right) ^{2}}{r}\end{displaymath}">|; 

$key = q/(D_{SA_{I}}=1-P_{SA_{I}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="127" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="\( D_{SA_{I}}=1-P_{SA_{I}} \)">|; 

$key = q/{displaymath}tilde{theta}_{U}=frac{1}{m}sum^{m}_{l=1}tilde{theta}_{l}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="54" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="\begin{displaymath}
\tilde{\theta }_{U}=\frac{1}{m}\sum ^{m}_{l=1}\tilde{\theta }_{l}\end{displaymath}">|; 

$key = q/(tilde{alpha}_{il}=1-sum^{v_{l}}_{u=1}tilde{p}_{ilu}^{2});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="141" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="\( \tilde{\alpha }_{il}=1-\sum ^{v_{l}}_{u=1}\tilde{p}_{ilu}^{2} \)">|; 

$key = q/(x=sum^{m}_{l=1}a_{l}b_{l});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="100" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="\( x=\sum ^{m}_{l=1}a_{l}b_{l} \)">|; 

$key = q/(D_{A});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="\( D_{A}\protect \)">|; 

$key = q/(V_{X}=frac{1}{r-1}left(sum^{r}_{j}mu_{X_{j}}^{2}-roverline{mu_{X}}^{2}right));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="204" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="\( V_{X}=\frac{1}{r-1}\left( \sum ^{r}_{j}\mu _{X_{j}}^{2}-r\overline{\mu _{X}}^{2}\right) \)">|; 

$key = q/(V_{Y}=frac{1}{r-1}left(sum^{r}_{j}mu_{Y_{j}}^{2}-roverline{mu_{Y}}^{2}right));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="199" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="\( V_{Y}=\frac{1}{r-1}\left( \sum ^{r}_{j}\mu _{Y_{j}}^{2}-r\overline{\mu _{Y}}^{2}\right) \)">|; 

$key = q/(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="\( i \)">|; 

$key = q/(V_{XY}=frac{1}{2}left(V_{X}+V_{Y}right));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="143" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="\( V_{XY}=\frac{1}{2}\left( V_{X}+V_{Y}\right) \)">|; 

$key = q/(tilde{theta}_{U});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="\( \tilde{\theta }_{U}\protect \)">|; 

$key = q/(J_{XY}=frac{sum^{r}_{j}sum^{m_{j}}_{i}x_{ij}y_{ij}}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="155" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="\( J_{XY}=\frac{\sum ^{r}_{j}\sum ^{m_{j}}_{i}x_{ij}y_{ij}}{r} \)">|; 

$key = q/(overline{P}_{SA_{B}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="\( \overline{P}_{SA_{B}} \)">|; 

$key = q/{displaymath}D_{SA_{B}}=1-frac{2overline{P}_{SA_{B}}}{overline{P}_{SA_{X}}+overline{P}_{SA_{Y}}}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="194" HEIGHT="48" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="\begin{displaymath}
D_{SA_{B}}=1-\frac{2\overline{P}_{SA_{B}}}{\overline{P}_{SA_{X}}+\overline{P}_{SA_{Y}}}\end{displaymath}">|; 

$key = q/(m_{j});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="\( m_{j} \)">|; 

$key = q/{displaymath}a_{l}+b_{l}=frac{1}{2}sum_{u}left(tilde{p}_{1lu}-tilde{p}_{2lu}righalpha}_{2l}right)}{4n_{1}n_{2}left(n_{1}+n_{2}-1right)}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="456" HEIGHT="49" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="\begin{displaymath}
a_{l}+b_{l}=\frac{1}{2}\sum _{u}\left( \tilde{p}_{1lu}-\tild...
...\alpha }_{2l}\right) }{4n_{1}n_{2}\left( n_{1}+n_{2}-1\right) }\end{displaymath}">|; 

$key = q/(bar{n}=sum^{r}_{i=1}frac{n_{i}}{r});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="\( \bar{n}=\sum ^{r}_{i=1}\frac{n_{i}}{r} \)">|; 

$key = q/{displaymath}tilde{theta}_{W}=frac{left(sum^{m}_{l=1}a_{l}right)}{sum^{m}_{l=1}left(a_{l}+b_{l}right)}{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="143" HEIGHT="46" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="\begin{displaymath}
\tilde{\theta }_{W}=\frac{\left( \sum ^{m}_{l=1}a_{l}\right) }{\sum ^{m}_{l=1}\left( a_{l}+b_{l}\right) }\end{displaymath}">|; 

1;

